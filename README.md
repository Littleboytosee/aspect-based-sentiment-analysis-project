# Aspect Based Sentiment Analysis Project

## Set up
1. If conda env already have running, should use:\
conda deactivate
2. cd in this folder:\
pip install virtualenv\
python3 -m venv env\
source env/bin/activate\
pip install -r requirements.txt
## Download weight
https://drive.google.com/drive/folders/1bcQVnmqAocJELXlYeSi6RyDHxldeyat4?usp=sharing 
Download a folder from the above link, place it into this project /dinh7_full_sa_weight
## Run server
python manage.py migrate\
python3 manage.py runserver --settings absaproject.local_with_cloud_sql_proxy\
See absaproject, file local (and settings if you launch server with this file), CREDENTIAL_GOOGLE_PATH have to point your own credential from Google API json file. Config it suit yourself!\
